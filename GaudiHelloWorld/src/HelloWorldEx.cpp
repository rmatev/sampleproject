#include "GaudiAlg/Consumer.h"

struct HelloWorldEx final : Gaudi::Functional::Consumer<void( ), Gaudi::Functional::Traits::BaseClass_t<Gaudi::Algorithm>> {
    HelloWorldEx( const std::string& name, ISvcLocator* svcLoc )
        : Consumer( name, svcLoc ) {}

    void operator()( ) const override {
      info() << "Hello World: Executing..." << endmsg;
    }
  };

DECLARE_COMPONENT(HelloWorldEx)